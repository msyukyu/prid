@echo off

set path=%~dp0\PortableGit-2.23.0-64-bit;%~dp0\PortableGit-2.23.0-64-bit\bin;%~dp0dotnet-sdk-2.2.401-win-x64;%~dp0VSCode-win32-x64-1.38.1;%path%
set path=%APPDATA%\npm;%~dp0\nodejs;%path%
set path=c:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\IDE;%path%
set HOME=%~dp0

rem powershell -windowstyle hidden -Command "&{devenv}"
start devenv.exe
